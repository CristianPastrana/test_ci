from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os
import xmlschema

#Iniciamos el servicio
app = Flask(__name__)


# Se obtiene la DATABASE_URI para la ubicacion actual de la base de datos (database.db)
PWD = os.path.abspath(os.curdir)
DATABASE_URI = 'sqlite:///{}/database.db'.format(PWD).replace(chr(92) , "/" )

#Seteamos la DATABASE_URI
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False 
db = SQLAlchemy(app)

#Seteamos el directorio donde se encuentra el XML Schema 
SCHEMA_WD = '{}/schema.xsd'.format(PWD).replace(chr(92) , "/" )
xs = xmlschema.XMLSchema(SCHEMA_WD)
