# TP2-Pastrana Cristian

**¿Que necesitamos?**
- Docker > https://docs.docker.com/docker-for-windows/install/
- Postman > https://www.postman.com/downloads/

**¿Como ejecuto el programa?**

- Clonamos el repositorio donde nos quede mas comodo:

- En ese mismo directorio ejecutamos:
>docker-compose up

- Acabamos de iniciar nuestra aplicacion, podremos hacer una prueba rapida ingresando la sig. url en el navegador:
> http://127.0.0.1:4000/rent/

- A continuacion podremos realizar un test a nuestra aplicacion, abrimos otra terminal y ejecutamos:
> docker exec API_videoclub pytest test_api.py -v

- En resumen, iniciamos la aplicacion, realizamos un test, nos queda utilizar Postman para realizar requests.

**¿Como relizo pruebas con Postman?**

- Abrimos el Postman.

- Importamos el archivo **Pruebas API.postman_collection**

- Nos aparecera un listado con los requests.

- Ejecutamos los requests.



