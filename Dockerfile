FROM python:2

#Seteamos el directorio de trabajo
WORKDIR /usr/src/app

#Copiamos todos los archivos de nuestro proyecto al directorio de trabajo (menos los que hayamos en el .dockerignore )
COPY . /usr/src/app

#Instalamos las dependencias de nuetro proyecto
RUN pip install --no-cache-dir -r requirements.txt

#Ejecutamos el comando que iniciara nuestra aplicacion
CMD [ "python", "/usr/src/app/server.py" ]