import pytest
import requests


@pytest.fixture
def client():
    import verbos
    
    verbos.app.config['TESTING'] = True
    return verbos.app.test_client()


def test_1_get_all_rents(client):
    xml= "<renta><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></renta>"
    response = client.get("/rent/", data= xml)
    print("status code esperado: 200")
    assert response.status_code == 200
    

def test_2_add_new_rent(client):
    xml= "<renta><object_id>bb72a2ab-5900-4532-bd68-aba0bbccddee</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></renta>"
    response = client.put("/rent/", data= xml)
    print("status code esperado: 201")
    assert response.status_code == 201

    
def test_3_update_new_rent(client):
    xml= "<renta><object_id>bb72a2ab-5900-4532-bd68-aba0bbccddee</object_id><client_id>5d65ac9e-1111-2222-3333-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></renta>"
    response = client.put("/rent/", data= xml)
    assert response.status_code == 204
    print("status code esperado: 204")


def test_4_add_new_rent_invalid_uuid (client):
    xml= "<renta><object_id>aaaaabbbbbccccccddddd</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></renta>"
    response = client.put("/rent/", data= xml)
    assert response.status_code == 400
    print("status code esperado: 400")

def test_5_add_new_rent_invalid_status (client):
    xml= "<renta><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENTED</status><until>2020/10/01</until></details></renta>"
    response = client.put("/rent/", data= xml)
    assert response.status_code == 400
    print("status code esperado: 400")

def test_6_add_new_rent_invalid_until (client):
    xml= "<renta><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>5d65ac9e-d431-4138-a8e4-c4719205cb1b</client_id><details><status>RENT</status><until>01/01/2001</until></details></renta>"
    response = client.put("/rent/", data= xml)
    assert response.status_code == 400
    print("status code esperado: 400")


def test_7_delete_rent(client):
    xml= "<renta><object_id>aa72a2ab-5900-4532-bd68-aba0bbccddee</object_id><client_id>5d65ac9e-1111-2222-3333-c4719205cb1b</client_id><details><status>RENT</status><until>2020/10/01</until></details></renta>"
    response = client.delete("/rent/bb72a2ab-5900-4532-bd68-aba0bbccddee", data= xml)
    assert response.status_code == 204
    print("status code esperado: 204")
    


