from flask import jsonify, Response, request
from servicios import app, db, xs
from entidades import *
import xml.etree.ElementTree as ET
import xmltodict
import uuid
from xml.parsers.expat import ExpatError


@app.route("/")
def server_info():
    rent= ET.Element("rent")
    server= ET.SubElement(rent,"server")
    server.text= "Trabajo Practico nro. 2"

    xml= ET.tostring(rent)

    return Response(xml, mimetype='text/xml')
 

@app.route("/rent/", methods=["GET"])
def get():
    rents = Rent.query.order_by(Rent.object_id).all()
    rentas = ET.Element("rentas")

    for x in rents:
        renta = ET.SubElement(rentas, "renta")
        ET.SubElement(renta, "object_id").text = str(x.object_id)
        ET.SubElement(renta, "client_id").text = str(x.client_id)
        details = ET.SubElement(renta, "details")
        ET.SubElement(details, "status").text = str(x.status)
        ET.SubElement(details, "until").text = str(x.until)

    xml = ET.tostring(rentas)

    return Response(xml, mimetype='text/xml'), 200
    

@app.route("/rent/", methods=["PUT"])
def new_rent():

    try:
        content_dict = xmltodict.parse(request.data)
    except ExpatError:
        return "XML vacio", 400 
    
    xml_valido= xs.is_valid(request.data)

    object_id = content_dict["renta"]["object_id"]
    client_id = content_dict["renta"]["client_id"]
    status = content_dict["renta"]["details"]["status"]
    until = content_dict["renta"]["details"]["until"]

   
    update_renta = Rent.query.get(object_id)
    if update_renta == None:
        nueva_renta = Rent()
        #Si el xml es valido respecto al schema se cargan los atributos en una nueva renta   
        if xml_valido:
            nueva_renta.object_id = object_id
            nueva_renta.client_id = client_id    
            nueva_renta.status = status
            nueva_renta.until = until
        else:
            return "xml invalido para el schema", 400
        
        db.session.add(nueva_renta)
        db.session.commit()
        xml= request.data
        return Response(xml, mimetype='text/xml'), 201
    else:
        #Si el xml es valido respecto al schema se actualizan los atributos en una renta existente
        if xs.is_valid(request.data):
            update_renta.object_id = object_id
            update_renta.client_id = client_id
            update_renta.status = status
            update_renta.until = until
        else:
            return "xml invalido para el schema", 400
        
        db.session.add(update_renta)
        db.session.commit()
        xml= request.data
        return Response(xml, mimetype='text/xml'), 204
        


@app.route("/rent/<id>", methods=["DELETE"])
def delete_renta(id):   
    renta = Rent.query.get(id)
    if renta == None:
        return  "no existe la renta que se quiere eliminar", 404
    else:
        db.session.delete(renta)
        db.session.commit()
        return id, 204



